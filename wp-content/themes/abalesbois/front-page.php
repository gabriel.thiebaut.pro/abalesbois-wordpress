<?php
get_header();
printf('%s', do_shortcode('[smartslider3 slider="2"]'));

the_content(); ?>
<h2>Activités</h2>
<div class="flex">
    <?php

    $args = array(
        'post_type' => 'activity',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
    );

    $loop = new WP_Query($args);

    while ($loop->have_posts()) : $loop->the_post(); ?>
        <a href="<?php the_permalink()?>">
            <div class="icon-card">
                <div class="icon-card-header">
                    <i class="<?php printf('%s', get_field("icon")); ?>"></i>
                </div>
                <div class="icon-card-title">
                    <h4><?php the_title(); ?></h4>
                </div>
            </div>
        </a>
    <?php
    endwhile;

    wp_reset_postdata();
    ?>
    </div>
    <h2>Engagements</h2>
    <div class="flex">
    <?php

    $args = array(
        'post_type' => 'engagements',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
    );

    $loop = new WP_Query($args);

    while ($loop->have_posts()) : $loop->the_post(); ?>
        <a href="<?php the_permalink()?>">
            <div class="icon-card">
                <div class="icon-card-header">
                    <i class="<?php printf('%s', get_field("icon")); ?>"></i>
                </div>
                <div class="icon-card-title">
                    <h4><?php the_title(); ?></h4>
                </div>
            </div>
        </a>
    <?php
    endwhile;

    wp_reset_postdata();
    ?>
    </div>
    <h2>Partenaires</h2>
    <div class="flex">
    <?php

//ajout partenaire dans Front-Page
$args = array(
    'post_type' => 'partenaires',
    'post_status' => 'publish',
    'orderby' => 'title', 
    'order' => 'ASC', 
);

$loop = new WP_Query( $args ); 

while ( $loop->have_posts() ) : $loop->the_post(); ?>
<div class="icon-card">
    <div class="icon-card-header">
        <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive', 'title' => 'Feature image']); ?>
    </div>
    <div class="icon-card-title">
        <h4><?php the_title(); ?></h4>
    </div>
</div>
<?php
endwhile;
wp_reset_postdata(); 

?>
</div>
<?php
 get_footer(); 
