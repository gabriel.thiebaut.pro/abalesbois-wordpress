<?php

/* Add scripts */
function ablb_add_scripts()
{

    //style.css
    wp_enqueue_style(
        'ablb-style',
        get_stylesheet_uri(),
        array(),
        '1.0'
    );

    // header css
    wp_enqueue_style(
        'ablb-header',
        get_template_directory_uri() . '/css/header.css',
        array(),
        '1.0'
    );

    // contact css
    wp_enqueue_style(
        'ablb-contact',
        get_template_directory_uri() . '/css/contact.css',
        array(),
        '1.0'
    );

        // footer css
        wp_enqueue_style(
            'ablb-footer',
            get_template_directory_uri() . '/css/footer.css',
            array(),
            '1.0'
        );


    wp_enqueue_style(
        'ablb-font-awesome',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css',
        array(),
        '1.0'
    );
    
    //si une page CPT
    if(is_archive()){
        //engagements et activity css
        wp_enqueue_style(
            'ablb-activity',
            get_template_directory_uri() . '/css/activity.css',
            array(),
            '1.0'
        );
    }    

    // single css
    wp_enqueue_style(
        'ablb-single',
        get_template_directory_uri() . '/css/single.css',
        array(),
        '1.0'
    );

    // gallery css
    wp_enqueue_style(
        'ablb-gallery',
        get_template_directory_uri() . '/css/gallery.css',
        array(),
        '1.0'
    );



    //si page 404
    if (is_404()) {
        // 404 css
        wp_enqueue_style(
            'ablb-404',
            get_template_directory_uri() . '/css/404.css',
            array(),
            '1.0'
        );
    }


    wp_enqueue_style(
        'ablb-formulaire',
        get_template_directory_uri() . '/css/formulaire.css',
        array(),
        '1.0'
    );

    //si page d'accueil
    if (is_front_page()) {
        // home css
        wp_enqueue_style(
            'ablb-home',
            get_template_directory_uri() . '/css/home.css',
            array(),
            '1.0'
        );
    }
}

add_action('wp_enqueue_scripts', 'ablb_add_scripts');
