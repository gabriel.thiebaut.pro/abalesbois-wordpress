<?php get_header(); ?>

<div class="under_header" style="background-image:url(<?php echo (get_template_directory_uri()."/src/bg.JPG" )?>);">
    <div class="under-header-calc">
        <h1>ENGAGEMENTS</h1>
    </div>
</div>

<?php if (have_posts()) : ?>

    <div class="container-cards">
        <?php while (have_posts()) : the_post(); ?>
            <div class="card">
                <img src="<?php the_post_thumbnail_url()?>" alt="Image" class="card-img">
                <div class="card-text">
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_content(); ?></p>
                </div>
                <div class="button-card">
                <a href="<?php the_permalink()?>"><button>Lire la suite</button></a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>

<?php endif; ?>

<?php get_footer(); ?>