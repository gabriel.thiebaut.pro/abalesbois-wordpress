<?php

/////Full Paretnaire
function ablb_custom_post_type_partenaire() {

    // On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
    $labels = array(
        // Le nom au pluriel
        'name'                => _x( 'Partenaires', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __( 'Partenaires'),
        // Les différents libellés de l'administration
        'all_items'           => __( 'Tout les Partenaires'),
        'view_item'           => __( 'Voir les Partenaires'),
        'add_new_item'        => __( 'Ajouter un nouveau Partenaire'),
        'add_new'             => __( 'Ajouter'),
        'edit_item'           => __( 'Editer un Partenaire'),
        'update_item'         => __( 'Modifier un Partenaire'),
        'search_items'        => __( 'Rechercher un Partenaire'),
        'not_found'           => __( 'Non trouvée'),
        'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
    );
    
    // On peut définir ici d'autres options pour notre custom post type
    
    $args = array(
        'label'               => __( 'Partenaires'),
        'description'         => __( 'Tous sur les Partenaires'),
        'labels'              => $labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre)
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        /* 
        * Différentes options supplémentaires
        */
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'			  => array( 'slug' => 'Partenaires'),
    
    );
    
    // On enregistre notre custom post type qu'on nomme ici "Partenaires" et ses arguments
    register_post_type( 'Partenaires', $args );
    add_theme_support( 'post-thumbnails', array( 'post', 'partenaires' ) );
    }
    
    add_action( 'after_setup_theme', 'ablb_custom_post_type_partenaire', 9 );







