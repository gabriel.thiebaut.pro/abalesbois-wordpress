<?php
function ablb_custom_post_type_engagements() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Engagements', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Engagement', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Engagements'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Tout les Engagements'),
		'view_item'           => __( 'Voir les Engagements'),
		'add_new_item'        => __( 'Ajouter un nouveau Engagement'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer un Engagement'),
		'update_item'         => __( 'Modifier un Engagement '),
		'search_items'        => __( 'Rechercher un Engagement'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	// On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( 'Engagements'),
		'description'         => __( 'Tous sur les Engagements'),
		'labels'              => $labels,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre)
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
		/* 
		* Différentes options supplémentaires
		*/
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'engagements'),

	);
	
	// On enregistre notre custom post type qu'on nomme ici "engagements" et ses arguments
	register_post_type( 'engagements', $args );

}

add_action( 'after_setup_theme', 'ablb_custom_post_type_engagements', 9 );
