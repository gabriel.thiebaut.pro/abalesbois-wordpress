<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>> 

<?php wp_body_open(); ?>
<header>
    <div class="container flex">
        <a href="<?php echo home_url(); ?>" class="brand">
            <?php printf('%s', get_bloginfo('name')); ?>
        </a>
    <?php
    //menu
    wp_nav_menu( array( 
            'theme_location' => 'main',
            'container' => 'div', 
            'menu_class' => 'header-menu', // ma classe personnalisée 
            ) ); ?>
    </div>
</header>

