<?php
get_header();

if ( have_posts()):
?>
	<?php the_content(); ?>
<?php
endif;
get_footer();

?>
