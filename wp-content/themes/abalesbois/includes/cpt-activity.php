<?php 

function ablb_custom_post_type_activty() {

// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
$labels = array(
    // Le nom au pluriel
    'name'                => _x( 'Activités', 'Post Type General Name'),
    // Le nom au singulier
    'singular_name'       => _x( 'Activité', 'Post Type Singular Name'),
    // Le libellé affiché dans le menu
    'menu_name'           => __( 'Activités'),
    // Les différents libellés de l'administration
    'all_items'           => __( 'Activités'),
    'view_item'           => __( 'Voir les Activités'),
    'add_new_item'        => __( 'Ajouter une nouvelle Activité'),
    'add_new'             => __( 'Ajouter'),
    'edit_item'           => __( 'Editer une Activité'),
    'update_item'         => __( 'Modifier une Activité '),
    'search_items'        => __( 'Rechercher une Activité'),
    'not_found'           => __( 'Non trouvée'),
    'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
);

// On peut définir ici d'autres options pour notre custom post type

$args = array(
    'label'               => __( 'Activités'),
    'description'         => __( 'Tout sur les Activités'),
    'labels'              => $labels,
    // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre)
    'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
    /* 
    * Différentes options supplémentaires
    */
    'show_in_rest' => true,
    'hierarchical'        => true,
    'public'              => true,
    'has_archive'         => true,
    'rewrite'			  => array( 'slug' => 'activity'),

);

// On enregistre notre custom post type qu'on nomme ici "engagements" et ses arguments
register_post_type( 'activity', $args );

}

add_action( 'after_setup_theme', 'ablb_custom_post_type_activty', 9 );

function ablb_set_custom_post_types_order($wp_query) {

    $post_type = $wp_query->query['post_type'];
    if ( in_array($post_type, array('activity', 'engagements'))) {
        // La valeur de 'orderby' peut être n'importe quelle colonne : name, slug, date, etc
        $wp_query->set('orderby', 'menu_order');
        // Les valeurs possibles sont 'DESC' ou 'ASC'
        $wp_query->set('order', 'ASC');
    }    
}

add_filter('pre_get_posts', 'ablb_set_custom_post_types_order');
