<?php
get_header();?>

<?php if ( have_posts()):?>

    <div class="googlemap">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.9479396469355!2d3.8605843158289606!3d44.923810677358034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b4acd852b9291d%3A0x71042f826bcab770!2sLes%20Granges%2C%2043370%20Le%20Brignon!5e1!3m2!1sfr!2sfr!4v1633350113612!5m2!1sfr!2sfr" width="100%" height="450" style="padding: 0; border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>

    <section class="contactinfo">
        <div class="contact-info-left">
            <p><i class="fas fa-home"></i><span><?php printf('%s', get_field("ville"));
?></span></p>
            <p><i class="fas fa-city"></i><span><?php printf('%s', get_field("code_postal_")); ?></span></p>
            <p><i class="fas fa-envelope"></i><span> <?php printf('%s', get_field("email_")); ?></span></p></p>
            <p><i class="fas fa-phone-alt"></i><span><?php printf('%s', get_field("numero_")); ?></span></p>
        </div>
        <?php the_content(); ?>
    </section>
	

<?php endif; ?>

<?php get_footer();?>