<?php
add_theme_support('post-thumbnails', array('post', 'partenaires', 'activity', 'engagements'));

require_once('includes/cpt-activity.php');
require_once('includes/cpt-engagements.php');
require_once('includes/add-scripts.php');
require_once('includes/cpt-partenaires.php');


register_nav_menus( array(
	'main' => 'Menu Principal'
) );

