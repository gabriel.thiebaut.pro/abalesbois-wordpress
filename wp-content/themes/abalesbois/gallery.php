<?php /* Template Name: Gallery Template */

get_header();

$query_images_args = array(
    'post_type'      => 'attachment',
    'post_mime_type' => 'image',
    'post_status'    => 'inherit',
    'posts_per_page' => -1,
);

$query_images = new WP_Query($query_images_args);

$images = array();

foreach ($query_images->posts as $image) {
    $images[] = wp_get_attachment_url($image->ID);
} ?>

<div class="container-gallery">
    <?php for ($i = 0; $i < count($images); $i++) : ?>
        <a href="<?php echo $images[$i] ?>">
            <div class="img-div">
                <img src="<?php echo $images[$i] ?>" alt="">
                <div class="calc">
                    <h2>Ouvrir</h2>
                </div>
            </div>
        </a>
    <?php endfor; ?>
</div>

<?php get_footer();
