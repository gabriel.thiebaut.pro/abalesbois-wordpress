<?php get_header() ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post() ?>
        <div class="under-header" style="background-image:url('<?php the_post_thumbnail_url() ?>');">
            <div class="under-header-calc">
                <h1><?php the_title() ?></h1>
            </div>
        </div>
        <div class="container ablb-single">
            <?php the_content() ?>    
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer() ?>