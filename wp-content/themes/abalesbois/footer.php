<footer>
    <div class="container flex">
        <div class="site-name">
            <p><?php printf('%s', get_bloginfo('name')); ?></p>
        </div>
        <div class="site-copyright">
            <p>© abalesbois.fr 2021. - Tous droits réservés</p>
        </div>
        <div class="site-socials">
            <?php
            //menu
            wp_nav_menu(array(
                'theme_location' => 'main',
                'container' => 'div',
                'menu_class' => 'header-menu', // ma classe personnalisée 
            )); ?>
        </div>
    </div>
</footer>

</body>

</html>